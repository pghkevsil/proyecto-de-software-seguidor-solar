##################################################################################
#######Cálculo de ángulo de altitud solar para un día en relación al tiempo#######
##################################################################################

import matplotlib.pyplot as plt   #Se importa una biblioteca para trabajar con gráficas y se le nombra plt
import numpy as np                #Se importa una biblioteca para trabajar con funciones y se le nombra np

#######Constantes#######
SL=-45                #Longitud estándar
LL=-73.24             #Longitud local
L=np.radians(-39.83)  #Latitud local
N= 1                  #Número del día
B=(N-81)*(360/364);
#######Ecuación del tiempo#######
ET=(9.87*np.sin(np.radians(2*B))-7.53*np.cos(np.radians(B))-1.5*np.sin(np.radians(B)));
#######Ángulo de inclinación#######
AD=23.45*np.sin(np.radians((360/365)*(284+N)))      

###########Variables###########
AAS=[]; #Se define una lista para los valores del ángulo de altitud solar
LSTG=np.arange(1,1440,1)   #Tiempo estándar local
for LST in range(1,1440):  #Para LST en el rango de 1 hasta 1440 se ejecuta:
    AST=LST+ET-4*(SL-LL);  #Tiempo solar aparente
    h=(AST-720)*0.25;      #Ángulo horario
    AAS.append(np.arcsin(np.sin(L)*np.sin(np.radians(AD))+np.cos(L)*np.cos(np.radians(AD))*np.cos(np.radians(h)))*(180/np.pi)); #Ángulo de altitud solar

plt.xlabel("Tiempo(horas)")  #Se nombra el eje x
plt.xlim(4,22)          #Se limita el eje y para ciertos valores
plt.ylim(0,90)          #Se limita el eje y para ciertos valores
plt.ylabel("Ángulo de altitud solar")   #Se nombra el eje y
plt.title("Ángulo de altitud solar en función del tiempo")   #Título de la gráfica
plt.plot(LSTG/60,AAS)   #Se hace la gráfica
plt.show()              #Se muestra la gráfica

