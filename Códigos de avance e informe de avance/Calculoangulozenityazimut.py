import matplotlib.pyplot as plt   #Biblioteca para trabajar con graficas, se le nombra plt
import numpy as np                #Biblioteca para trabajar con funciones, se le nombra np
import threading                  #Biblioteca para subprocesamiento
import time
from datetime import datetime     #Biblioteca para trabajar con tiempo
r=np.radians

N = datetime.now().timetuple().tm_yday  #Muestra el día actual

#######Constantes#######
SL=-45                #Longitud estandar
LL=-73.24             #Longitud local
L=r(-39.8)            #Latitud local
B=(N-81)*(360/364);
#######Ecuacion del tiempo#######
ET=(9.87*np.sin(r(2*B))-7.53*np.cos(r(B))-1.5*np.sin(r(B)));
#######Angulo de declinacion#######
AD=23.45*np.sin(r((360/365)*(284+N))) 

def timer(timer_runs):
    while timer_runs.is_set():
        mins=datetime.now()  #Muestra el tiempo actual
        hem=mins.hour*60     #Convierte las horas del tiempo actual en mins
        LST=hem+mins.minute  #Tiempo actual en mins, horas mas minutos
        AST=LST+ET-4*(SL-LL);  #Tiempo solar aparente
        h=(AST-720)*0.25;      #Angulo horario
        #Angulo de altitud solar
        AAS=np.arcsin(np.sin(L)*np.sin(r(AD))+np.cos(L)*np.cos(r(AD))*np.cos(r(h)))*(180/np.pi); 
        #Angulo cenital
        ACS=90-AAS
        #Angulo de azimut
        AA=(np.arcsin((np.cos(r(AD))*np.sin(r(h)))/(np.cos(r(AAS))))*(180/np.pi));
        print('Angulo cenital solar: ', ACS) #Entrega el angulo cenital solar
        print('Angulo de azimut solar: ', AA)  #Entrega el angulo de azimut solar
        time.sleep(60)   #Tiempo cada cuanto se haran los calculos(60 segundos)
timer_runs = threading.Event()
timer_runs.set()
t = threading.Thread(target=timer, args=(timer_runs,))
t.start()  #Empieza el timer
