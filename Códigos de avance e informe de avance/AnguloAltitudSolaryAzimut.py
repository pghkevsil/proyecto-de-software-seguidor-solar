import matplotlib.pyplot as plt   #Biblioteca para trabajar con graficas, se le nombra plt
import numpy as np                #Biblioteca para trabajar con funciones, se le nombra np
r=np.radians

#######Constantes#######
SL=-45                #Longitud estandar
LL=-73.24             #Longitud local
L=r(-39.8)            #Latitud local
N=356                 #Numero del dia
B=(N-81)*(360/364);
#######Ecuacion del tiempo#######
ET=(9.87*np.sin(r(2*B))-7.53*np.cos(r(B))-1.5*np.sin(r(B)));
#######Angulo de declinacion#######
AD=23.45*np.sin(r((360/365)*(284+N)))      

###########Variables###########
AnAS=[]; #Se define una lista para los valores del angulo de altitud solar
AA=[];   #Se define una lista para los valores del angulo de azimut
LSTG=np.arange(1,1440,1)   #Tiempo estandar local
for LST in range(1,1440):  #Para LST en el rango de 1 hasta 1440 se ejecuta:
    AST=LST+ET-4*(SL-LL);  #Tiempo solar aparente
    h=(AST-720)*0.25;      #Angulo horario
    #Angulo de altitud solar
    AAS=np.arcsin(np.sin(L)*np.sin(r(AD))+np.cos(L)*np.cos(r(AD))*np.cos(r(h)))*(180/np.pi); 
    AnAS.append(AAS)       #Se agregan los valores
    #Angulo de azimut
    AA.append(np.arcsin((np.cos(r(AD))*np.sin(r(h)))/(np.cos(r(AAS))))*(180/np.pi)); 

fig, ax = plt.subplots()
plt.xlabel("Tiempo(horas)")  #Se nombra el eje x
plt.xlim(4,22)               #Se limita el eje x entre 4 y 22 horas
plt.ylabel("Grados")         #Se nombra el eje y
plt.ylim(-90,90)             #Se limita el eje y entre -90° y 90°
plt.title("angulo de azimut y de altitud solar en funcion del tiempo")#Titulo de la grafica
line1, = ax.plot(LSTG/60,AA)   #Grafica de angulo de azimut vs tiempo
line2, = ax.plot(LSTG/60,AnAS) #Grafica de angulo de altitud solar vs tiempo
ax.legend(['angulo de Azimut', 'angulo de Altitud'],loc='upper left')#Leyendas del grafico
plt.show()             #Se muestra la grafica
