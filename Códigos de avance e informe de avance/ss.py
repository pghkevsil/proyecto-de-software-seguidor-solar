############################################
#######Cálculo de ecuación del tiempo#######
############################################

import matplotlib.pyplot as plt   #Se importa una biblioteca para trabajar con gráficas y se le nombra plt
import numpy as np                #Se importa una biblioteca para trabajar con funciones y se le nombra np

#Ecuación del tiempo
ET = []                #Se crea una lista para los valores que tendrá ET
A=np.arange(1,365,1)   #Días de 0 a 365
for N in range(1,365): #Para N hasta 365(días) ejecuta:
    B=(N-81)*(360/364); 
    ET.append(9.87*np.sin(np.radians(2*B))-7.53*np.cos(np.radians(B))-1.5*np.sin(np.radians(B))); #Cálculo ecuación del tiempo
plt.plot(A,ET)  #Se gráfica
plt.xlabel("Número de día") #Se nombre el eje x
plt.ylabel("Minutos")       #Se nombre el eje y
plt.title("Ecuación del tiempo para 365 días")  #Título del gráfico
plt.show() #Se muestra el gráfico


