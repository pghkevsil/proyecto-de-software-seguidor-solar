########################################################################
#######Cálculo de ángulo azimut para un día en relación al tiempo#######
########################################################################

import matplotlib.pyplot as plt   #Se importa una biblioteca para trabajar con gráficas y se le nombra plt
import numpy as np                #Se importa una biblioteca para trabajar con funciones y se le nombra np

#######Constantes#######
SL=-45                #Longitud estándar
LL=-73.24             #Longitud local
L=np.radians(-39.8)   #Latitud local
N=1                   #Número del día
B=(N-81)*(360/364);
#######Ecuación del tiempo#######
ET=(9.87*np.sin(np.radians(2*B))-7.53*np.cos(np.radians(B))-1.5*np.sin(np.radians(B)));
#######Ángulo de inclinación#######
AD=23.45*np.sin(np.radians((360/365)*(284+N)))      

###########Variables###########
AA=[]; #Se define una lista para los valores del ángulo de azimut
LSTG=np.arange(1,1440,1)   #Tiempo estándar local
for LST in range(1,1440):  #Para LST en el rango de 1 hasta 1440 se ejecuta:
    AST=LST+ET-4*(SL-LL);  #Tiempo solar aparente
    h=(AST-720)*0.25;      #Ángulo horario
    AAS=np.arcsin(np.sin(L)*np.sin(np.radians(AD))+np.cos(L)*np.cos(np.radians(AD))*np.cos(np.radians(h)))*(180/np.pi); #Ángulo de altitud solar
    AA.append(np.arcsin((np.cos(np.radians(AD))*np.sin(np.radians(h)))/(np.cos(np.radians(AAS))))*(180/np.pi)); #Ángulo de azimut
print(AA)    
plt.xlabel("Tiempo(mins)")  #Se nombra el eje x
plt.xlim(4,22)              #Se limita el eje x para ciertos valores
plt.ylim(-90,90)            #Se limita el eje y para ciertos valores
plt.ylabel("Ángulo de azimut")   #Se nombra el eje y
plt.title("Ángulo de azimut solar en función del tiempo")   #Título de la gráfica
plt.plot(LSTG/60,AA)   #Se hace la gráfica
plt.show()             #Se muestra la gráfica

     
 
                
